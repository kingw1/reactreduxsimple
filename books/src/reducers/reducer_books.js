export default function () {
    return [
        {title: 'Javascript', pages: 101},
        {title: 'Harray Porter', pages: 98},
        {title: 'The Dark Tower', pages: 204},
        {title: 'Ruby on Rails', pages: 45},
        {title: 'PHP Expert', pages: 76},
    ]
}